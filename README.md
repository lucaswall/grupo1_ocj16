# One Click Jam 2016 Grupo 1 #

Escape from your comfort zone jumping over obstacles and get the best score.

This game was made for the One Click Jam 16. Made in the course of 48hs from scratch.

http://www.oneclickjam.org/

Run and jump your way out of your comfort zone avoiding the comfy obstacles that come towards you. If you crash into them you will loose speed and get closer to your comfort zone. If the comfort zone catches up with you, you will loose a life.

Experimental. Activate the microphone and use your own voice to control the game.


# Authors

- Federico Terpin
- Guido Ferrari
- Mariana Carril
- Lucas Wall
- Exequiel Rodriguez
- Francisco "Cob" Nuñez
