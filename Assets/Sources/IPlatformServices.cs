using UnityEngine;
using System.Collections;

public interface IPlatformServices {

	void Update();
	void StartRecording();
	void StopRecording();
	int GetRecordingMaxAmplitude();
	bool MicSupport();

}
