﻿using UnityEngine;
using System.Collections;

public class PersistentState {

	static bool init = false;
	static int lastScore = 0;
	static bool micInput = false;

	const string KEY_MAXSCORE = "MAX_SCORE";
	const string KEY_MICINPUT = "MIC_INPUT";

	static public int LastScore {
		get { return lastScore; }
		set {
			lastScore = value;
			int maxScore = PlayerPrefs.GetInt(KEY_MAXSCORE);
			if ( lastScore > maxScore ) {
				PlayerPrefs.SetInt(KEY_MAXSCORE, lastScore);
			}
		}
	}

	static public int MaxScore {
		get { return PlayerPrefs.GetInt(KEY_MAXSCORE); }
	}

	static public bool MicInput {
		get { Init(); return micInput; }
		set { PlayerPrefs.SetInt(KEY_MICINPUT, value ? 1 : 0); micInput = value; }
	}

	static void Init() {
		if ( init ) return;
		micInput = PlayerPrefs.GetInt(KEY_MICINPUT) != 0;
		init = true;
	}

}
