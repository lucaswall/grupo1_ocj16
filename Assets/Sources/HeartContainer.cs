﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HeartContainer : MonoBehaviour {

	public Image[] heartImages;
	public Sprite heartOn;
	public Sprite heartOff;

	int life;

	public bool IsDead {
		get { return life == 0; }
	}

	void Start() {
		life = heartImages.Length;
	}

	public void LooseHeart() {
		if ( life <= 0 ) return;
		life--;
		heartImages[life].sprite = heartOff;
	}

}
