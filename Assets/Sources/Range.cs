using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Range {
	public float min;
	public float max;

	public float Length {
		get { return max - min; }
	}

	public Range() {
		min = 0.0f;
		max = 0.0f;
	}

	public Range(float min, float max) {
		this.min = min;
		this.max = max;
	}

	public float GetRandomValue() {
		return UnityEngine.Random.Range(min, max);
	}

	public float GetInterpolatedValue(float f) {
		return min + (max - min) * f;
	}

	static public Range Lerp(Range a, Range b, float t) {
		return new Range(Mathf.Lerp(a.min, b.min, t), Mathf.Lerp(a.max, b.max, t));
	}

}
