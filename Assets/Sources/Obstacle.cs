﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

	public SpriteRenderer spriteRenderer;
	public Collider2D spriteCollider;
	public float destroyTime;
	public float fadeOutTime;
	public CameraShake cameraShake;
	public ObstacleMover mover;

	public void CollidedWithPlayer() {
		spriteCollider.enabled = false;
		Destroy(gameObject, destroyTime);
		StartCoroutine(FadeOut());
		cameraShake.Shake();
	}

	public void StopMovement() {
		mover.enabled = false;
	}

	IEnumerator FadeOut() {
		Color c = spriteRenderer.color;
		c.a = 1.0f;
		while ( c.a > 0.0f ) {
			spriteRenderer.color = c;
			c.a -= Time.unscaledDeltaTime / fadeOutTime;
			yield return null;
		}
		c.a = 0.0f;
		spriteRenderer.color = c;
	}

}
