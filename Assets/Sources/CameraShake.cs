﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {

	public float shakeAmplitude;
	public float shakeTime;

	Vector3 originalPosition;
	float time;

	void Start() {
		originalPosition = transform.position;
		time = 0.0f;
	}

	void Update() {
		if ( time > 0.0f ) {
			time -= Time.unscaledDeltaTime;
			if ( time <= 0.0f ) {
				time = 0.0f;
				transform.position = originalPosition;
				Time.timeScale = 1.0f;
			} else {
				Vector3 pos = Random.insideUnitCircle * shakeAmplitude;
				pos.z = originalPosition.z;
				transform.position = pos;
			}
		}
	}

	public void Shake() {
		time = shakeTime;
	}

}
