﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class GeneralDirector : MonoBehaviour {

	public Image blackCurtain;
	public float fadeToGameoverTime;

	void OnEnable() {
		GameEvents.OnGameOver += OnGameOver;
	}

	void OnDisable() {
		GameEvents.OnGameOver -= OnGameOver;
	}

	void OnGameOver() {
		StartCoroutine(FadeToGameover());
	}

	IEnumerator FadeToGameover() {
		Color c = blackCurtain.color;
		c.a = 0.0f;
		blackCurtain.color = c;
		blackCurtain.enabled = true;
		float t = 0.0f;
		while ( t < 1.0f ) {
			c.a = Mathf.Lerp(0.0f, 1.0f, t);
			blackCurtain.color = c;
			t += Time.deltaTime / fadeToGameoverTime;
			yield return null;
		}
		c.a = 1.0f;
		blackCurtain.color = c;
		SceneManager.LoadScene("GameOver");
	}

}
