﻿using UnityEngine;
using System.Collections;

public class MoveAndWrap : MonoBehaviour {

	public float speed;
	public float wrapPos;
	public float wrapTo;

	void Update() {
		transform.Translate(new Vector3(-speed * Time.deltaTime, 0.0f, 0.0f));
		if ( transform.position.x <= wrapPos ) {
			transform.position = new Vector2(wrapTo, transform.position.y);
		}
	}

}
