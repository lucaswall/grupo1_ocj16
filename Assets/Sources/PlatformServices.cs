﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlatformServices : MonoBehaviour {

	public float micSamplingTime;
	public float activationAmplitude;

	IPlatformServices platformServices;
	bool recording = false;
	bool trigger = false;

	public bool IsTriggerAndReset() {
		bool t = trigger;
		trigger = false;
		return t;
	}

	public bool MicSupport() {
		return platformServices.MicSupport();
	}

	void Awake() {
#if UNITY_ANDROID && ! UNITY_EDITOR
		platformServices = new PlatformServicesAndroid();
#else
		platformServices = new PlatformServicesDummy();
#endif
	}

	public void StartRecording() {
		platformServices.StartRecording();
		recording = true;
		StartCoroutine(MicAmplitudeSampling());
	}

	public void StopRecording() {
		if ( recording ) {
			recording = false;
			platformServices.StopRecording();
		}
	}

	void OnDisable() {
		StopRecording();
	}

	void Update() {
		platformServices.Update();
	}

	IEnumerator MicAmplitudeSampling() {
		while ( recording ) {
			yield return new WaitForSeconds(micSamplingTime);
			int micMaxAmplitude = platformServices.GetRecordingMaxAmplitude();
			//Debug.Log(micMaxAmplitude);
			if ( micMaxAmplitude > activationAmplitude ) trigger = true;
		}
	}

}
