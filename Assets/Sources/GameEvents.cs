﻿using UnityEngine;
using System.Collections;

public class GameEvents {

	public static event UpdateSpeedAction OnUpdateSpeed;
	public static event GameOverAction OnGameOver;
	public static event UpdateSpeedFactorAction	OnUpdateSpeedFactor;

	public delegate void UpdateSpeedAction(float speed);
	public delegate void GameOverAction();
	public delegate void UpdateSpeedFactorAction(float speedFactor);

	public static void UpdateSpeed(float speed) {
		if ( OnUpdateSpeed != null ) OnUpdateSpeed(speed);
	}

	public static void GameOver() {
		if ( OnGameOver != null ) OnGameOver();
	}

	public static void UpdateSpeedFactor(float speedFactor) {
		if ( OnUpdateSpeedFactor != null ) OnUpdateSpeedFactor(speedFactor);
	}
}
