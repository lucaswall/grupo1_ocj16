﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class TapToStart : MonoBehaviour {

	public Text tapToStartText;
	public string nextScene;
	public float blinkTime;
	public float startAllowDelay;

	float nextBlink;
	float allowDelay;

	void Start() {
		nextBlink = blinkTime;
		allowDelay = startAllowDelay;
	}

	void Update() {
		TextBlink();
		CheckTapToStart();
	}

	void TextBlink() {
		nextBlink -= Time.deltaTime;
		if ( nextBlink <= 0.0f ) {
			nextBlink = blinkTime;
			tapToStartText.enabled = ! tapToStartText.enabled;
		}
	}

	void CheckTapToStart() {
		if ( allowDelay > 0.0f ) {
			allowDelay -= Time.deltaTime;
		}
		if ( allowDelay <= 0.0f && Input.GetButtonDown("Fire1") ) {
			SceneManager.LoadScene(nextScene);
		}
	}

}
