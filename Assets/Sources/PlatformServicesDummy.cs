using UnityEngine;
using System.Collections;

public class PlatformServicesDummy : IPlatformServices {

	int maxAmplitude = 0;

	public void Update() {
		if ( Input.GetKey(KeyCode.Alpha5) ) maxAmplitude = 20000;
		if ( Input.GetKey(KeyCode.Alpha4) ) maxAmplitude = 15000;
		if ( Input.GetKey(KeyCode.Alpha3) ) maxAmplitude = 10000;
		if ( Input.GetKey(KeyCode.Alpha2) ) maxAmplitude = 05000;
		if ( Input.GetKey(KeyCode.Alpha1) ) maxAmplitude = 02500;
	}

	public void StartRecording() {
		Debug.Log("Start recording.");
	}

	public void StopRecording() {
		Debug.Log("Stop recording.");
	}

	public int GetRecordingMaxAmplitude() {
		int amp = maxAmplitude;
		maxAmplitude = 0;
		return amp;
	}

	public bool MicSupport() {
#if UNITY_ANDROID
		return true;
#else
		return false;
#endif
	}

}
