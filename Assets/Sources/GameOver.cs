﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class GameOver : MonoBehaviour {

	public Text lastScore;
	public Text topScore;

	void Start() {
		lastScore.text = String.Format("YOUR SCORE WAS {0}", PersistentState.LastScore);
		topScore.text = String.Format("YOUR HIGHEST SCORE IS {0}", PersistentState.MaxScore);
	}

}
