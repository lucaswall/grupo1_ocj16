﻿using UnityEngine;
using System.Collections;

public class StepsSound : MonoBehaviour {

	public AudioSource source;
	public AudioClip[] clips;
	public Range interval;

	float nextSound;
	float speedFactor;

	void OnEnable() {
		GameEvents.OnUpdateSpeedFactor += OnUpdateSpeedFactor;
	}

	void OnDisable() {
		GameEvents.OnUpdateSpeedFactor -= OnUpdateSpeedFactor;
	}

	void Start() {
		nextSound = interval.GetInterpolatedValue(speedFactor);
	}	

	void Update() {
		nextSound -= Time.deltaTime;
		if ( nextSound <= 0.0f ) {
			nextSound = interval.GetInterpolatedValue(speedFactor);
			source.PlayOneShot(clips[Random.Range(0, clips.Length)]);
		}
	}

	void OnUpdateSpeedFactor(float speedFactor) {
		this.speedFactor = speedFactor;
	}
}
