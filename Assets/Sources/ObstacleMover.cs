﻿using UnityEngine;
using System.Collections;

public class ObstacleMover : MonoBehaviour {

	public float speed;
	public float outOfScreenPosition;

	void OnEnable() {
		GameEvents.OnUpdateSpeed += OnUpdateSpeed;
	}

	void OnDisable() {
		GameEvents.OnUpdateSpeed -= OnUpdateSpeed;
	}

	void Update() {
		CheckForDestroy();
		MoveObject();
	}

	void MoveObject() {
		transform.Translate(-speed * Time.deltaTime, 0.0f, 0.0f);
	}

	void CheckForDestroy() {
		if ( transform.position.x < outOfScreenPosition ) {
			DestroyObject(gameObject);
		}
	}

	void OnUpdateSpeed(float speed) {
		this.speed = speed;
	}

}
