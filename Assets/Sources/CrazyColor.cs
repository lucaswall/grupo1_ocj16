﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CrazyColor : MonoBehaviour {

	public Image targetImage;
	public float crazySpeed;

	Color originalColor;
	float nextChange;

	void Start() {
		originalColor = targetImage.color;
		enabled = false;
	}

	void Update() {
		if ( nextChange > 0.0f ) {
			nextChange -= Time.deltaTime;
			if ( nextChange <= 0.0f ) {
				nextChange = crazySpeed;
				targetImage.color = Random.ColorHSV();
			}
		}
	}

	void OnEnable() {
		nextChange = crazySpeed;
	}

	void OnDisable() {
		nextChange = 0.0f;
		targetImage.color = originalColor;
	}

}
