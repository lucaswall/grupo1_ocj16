﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour {

	public Image blackCurtain;
	public Text quote;
	public float curtainTime;
	public float quoteInTime;
	public float quoteDelayTime;

	bool started = false;

	void Start() {
		quote.enabled = false;
	}

	public void StartButton() {
		if ( started ) return;
		started = true;
		StartCoroutine(StartGame());
	}

	public void CreditsButton() {
		SceneManager.LoadScene("Credits");
	}

	IEnumerator StartGame() {

		Color c = blackCurtain.color;
		c.a = 0.0f;
		blackCurtain.enabled = true;
		float t = 0.0f;
		while ( t < 1.0f ) {
			t += Time.deltaTime / curtainTime;
			c.a = Mathf.Lerp(0.0f, 1.0f, t);
			blackCurtain.color = c;
			yield return null;
		}

		t = 0.0f;
		c = quote.color;
		c.a = 0.0f;
		quote.enabled = true;
		quote.color = c;
		while ( t < 1.0f ) {
			t += Time.deltaTime / quoteInTime;
			c.a = Mathf.Lerp(0.0f, 1.0f, t);
			quote.color = c;
			yield return null;
		}

		t = 0.0f;
		while ( t < quoteDelayTime ) {
			t += Time.deltaTime;
			if ( Input.GetButtonDown("Fire1") ) {
				break;
			}
			yield return null;
		}
		SceneManager.LoadScene("Main");
	}

}
