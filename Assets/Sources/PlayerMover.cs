﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerMover : MonoBehaviour {

	public Animator animator;
	public Animator sillonAnimator;
	public Range movePosition;
	public Range speedRange;
	public Toggle micToggle;
	public PlatformServices platformServices;
	public float advanceSpeed;
	public float crashSpeedReduce;
	public float speedFactorInitial;
	public Image speedBar;
	public float crashTime;
	public float recoverTime;
	public float abortJumpTime;
	public float crazySpeedLimit;
	public CrazyColor crazyColorBar;

	public float jumpPositionIncrement;
	public float jumpMoveTime;
	public Range jumpSustainTime;

	public Text scoreText;
	public HeartContainer heartContainer;

	public float scoreTime;
	public Range scoreRange;

	public PlayerInvincible playerInvincible;

	public float hitStateTime;
	public float hitDelay;
	public float onHitDelay;

	public AudioSource audioJump;
	public AudioSource audioCollide;
	public AudioSource audioSillon;
	public StepsSound stepsSound;
	public AudioSource audioPunch;

	int score = 0;
	float speedFactor;
	bool advancing = true;
	bool jumping = false;
	bool isAlive = true;
	float scoreNextUpdate;
	IEnumerator jumpCoroutine;
	Vector3 originalPosition;
	float nextHitTime = 0.0f;
	float hitTime = 0.0f;

	void Start() {
		originalPosition = transform.position;
		UpdateSpeedFactor(speedFactorInitial);
		scoreNextUpdate = scoreTime;
		micToggle.isOn = PersistentState.MicInput;
		if ( ! platformServices.MicSupport() ) {
			micToggle.gameObject.SetActive(false);
		}
		if ( PersistentState.MicInput ) {
			platformServices.StartRecording();
		}
	}

	void Update() {
		CheckFire();
		if ( advancing ) UpdateAdvanceSpeed();
		if ( isAlive ) UpdateScore();
		CheckPlayerHitState();
	}

	void CheckPlayerHitState() {
		if ( hitTime > 0.0f ) {
			hitTime -= Time.deltaTime;
			if ( hitTime <= 0.0f ) {
				EndShoot();
			}
		}
		if ( nextHitTime > 0.0f ) {
			nextHitTime -= Time.deltaTime;
		}
	}

	void EndShoot() {
		hitTime = 0.0f;
		animator.SetTrigger("Endshoot");
		nextHitTime = hitDelay;
	}

	void UpdateScore() {
		scoreNextUpdate -= Time.deltaTime;
		if ( scoreNextUpdate <= 0.0f ) {
			scoreNextUpdate = scoreTime;
			score += (int) scoreRange.GetInterpolatedValue(speedFactor) ;
			scoreText.text = score.ToString();
		}
	}

	void CheckFire() {
		if ( Input.GetButtonDown("Fire1")
				|| ( platformServices.IsTriggerAndReset() && PersistentState.MicInput ) ) {
			if ( speedFactor > crazySpeedLimit ) {
				if ( nextHitTime <= 0.0f ) {
					nextHitTime = hitStateTime;
					hitTime = hitStateTime;
					animator.SetTrigger("Shoot");
				}
			} else {
				Jump();
			}
		}
	}

	void UpdateAdvanceSpeed() {
		speedFactor += advanceSpeed * Time.deltaTime;
		if ( speedFactor > 1.0f ) speedFactor = 1.0f;
		UpdateSpeedFactor(speedFactor);
	}

	void OnTriggerEnter2D(Collider2D other) {
		Obstacle obs = other.GetComponent<Obstacle>();
		if ( obs != null ) {
			obs.CollidedWithPlayer();
			if ( hitTime > 0.0f ) {
				KilledObstacle(obs);
			} else {
				CollidedWithObstacle();
			}
		}
	}

	void KilledObstacle(Obstacle obs) {
		obs.StopMovement();
		Time.timeScale = 0.0f;
		hitTime = onHitDelay;
		audioPunch.Play();
	}

	void CollidedWithObstacle() {
		StartCoroutine(PushToPosition(Mathf.Clamp01(speedFactor - crashSpeedReduce), crashTime));
		if ( jumping ) {
			StopCoroutine(jumpCoroutine);
			StartCoroutine(MoveToOriginalPosition());
			animator.SetTrigger("EndJump");
			jumping = false;
			stepsSound.enabled = true;
		}
		animator.SetTrigger("Hit");
		audioCollide.Play();
	}

	IEnumerator MoveToOriginalPosition() {
		float posStart = transform.position.y;
		float posEnd = originalPosition.y;
		float t = 0.0f;
		while ( t <= 1.0f ) {
			t += Time.deltaTime / abortJumpTime;
			float y = Mathf.Lerp(posStart, posEnd, t);
			transform.position = new Vector2(transform.position.x, y);
			yield return null;
		}
	}

	IEnumerator PushToPosition(float to, float inTime) {
		float from = speedFactor;
		float t = 0;
		advancing = false;
		while ( t <= 1.0f ) {
			t += Time.deltaTime / inTime;
			speedFactor = Mathf.SmoothStep(from, to, t);
			UpdateSpeedFactor(speedFactor);
			yield return null;
		}
		speedFactor = to;
		UpdateSpeedFactor(speedFactor);
		if ( to <= 0.0f ) {
			PushedAllWayBack();
		} else {
			advancing = true;
		}
	}

	void PushedAllWayBack() {
		heartContainer.LooseHeart();
		if ( heartContainer.IsDead ) {
			isAlive = false;
			PersistentState.LastScore = score;
			GameEvents.GameOver();
		} else {
			sillonAnimator.SetTrigger("Hit");
			audioSillon.Play();
			playerInvincible.Activate();
			StartCoroutine(PushToPosition(speedFactorInitial, recoverTime));
		}
	}

	void Jump() {
		if ( jumping ) return;
		jumpCoroutine = DoJump();
		StartCoroutine(jumpCoroutine);
		audioJump.Play();
	}

	IEnumerator DoJump() {
		jumping = true;
		float posStart = originalPosition.y;
		float posEnd = posStart + jumpPositionIncrement;
		animator.SetTrigger("Jump");
		stepsSound.enabled = false;

		float t = 0.0f;
		while ( t <= 1.0f ) {
			t += Time.deltaTime / jumpMoveTime;
			float y = Mathf.SmoothStep(posStart, posEnd, t);
			transform.position = new Vector2(transform.position.x, y);
			yield return null;
		}

		yield return new WaitForSeconds(jumpSustainTime.GetInterpolatedValue(speedFactor));

		t = 0.0f;
		while ( t <= 1.0f ) {
			t += Time.deltaTime / jumpMoveTime;
			float y = Mathf.SmoothStep(posEnd, posStart, t);
			transform.position = new Vector2(transform.position.x, y);
			yield return null;
		}

		animator.SetTrigger("EndJump");
		jumping = false;
		stepsSound.enabled = true;
	}

	void UpdateSpeedFactor(float f) {
		speedFactor = f;
		speedBar.fillAmount = speedFactor;
		transform.position = new Vector2(movePosition.GetInterpolatedValue(speedFactor), transform.position.y);
		GameEvents.UpdateSpeed(speedRange.GetInterpolatedValue(speedFactor));
		GameEvents.UpdateSpeedFactor(speedFactor);
		if ( speedFactor > crazySpeedLimit ) {
			crazyColorBar.enabled = true;
		} else {
			crazyColorBar.enabled = false;
		}
	}

	public void MicToggleUpdate() {
		Debug.Log("Mic state is now " + (micToggle.isOn ? "ON" : "OFF"));
		PersistentState.MicInput = micToggle.isOn;
		if ( PersistentState.MicInput ) {
			platformServices.StartRecording();
		} else {
			platformServices.StopRecording();
		}
	}

}
