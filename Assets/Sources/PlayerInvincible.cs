﻿using UnityEngine;
using System.Collections;

public class PlayerInvincible : MonoBehaviour {

	public Collider2D playerCollider;
	public SpriteRenderer playerRenderer;
	public float invincibleTime;
	public float blinkSpeed;
	public float blinkAlpha;

	public void Activate() {
		StartCoroutine(DoInvincible());
	}

	IEnumerator DoInvincible() {
		playerCollider.enabled = false;
		float timeout = invincibleTime;
		float blink = blinkSpeed;
		Color c = playerRenderer.color;
		bool inBlink = false;
		while ( timeout > 0.0f ) {
			timeout -= Time.deltaTime;
			blink -= Time.deltaTime;
			if ( blink <= 0.0f ) {
				blink = blinkSpeed;
				inBlink = ! inBlink;
				c.a = inBlink ? blinkAlpha : 1.0f;
				playerRenderer.color = c;
			}
			yield return null;
		}
		playerCollider.enabled = true;
		playerRenderer.enabled = true;
		c.a = 1.0f;
		playerRenderer.color = c;
	}

}
