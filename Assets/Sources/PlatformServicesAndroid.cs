using UnityEngine;
using System.Collections;

#if UNITY_ANDROID

public class PlatformServicesAndroid : IPlatformServices {

	AndroidJavaObject javaObj;

	public PlatformServicesAndroid() {
		javaObj = new AndroidJavaObject("ar.com.kadath.pixel.AndroidSupport");
	}

	public void Update() {
	}

	public void StartRecording() {
		javaObj.Call("startRecording");
	}

	public void StopRecording() {
		javaObj.Call("stopRecording");
	}

	public int GetRecordingMaxAmplitude() {
		return javaObj.Call<int>("getRecordingMaxAmplitude");
	}

	public bool MicSupport() {
		return true;
	}

}

#endif
