﻿using UnityEngine;
using System.Collections;

public class ObjectSpawner : MonoBehaviour {

	public Obstacle[] obstacles;
	public Range spawnTime;
	public CameraShake cameraShake;

	float nextObjectTime;

	void Start() {
		nextObjectTime = spawnTime.GetRandomValue();
	}

	void Update() {
		nextObjectTime -= Time.deltaTime;
		if ( nextObjectTime <= 0.0f ) {
			nextObjectTime = spawnTime.GetRandomValue();
			Obstacle obs = Instantiate(GetRandomObstaclePrefab(), transform.position, Quaternion.identity) as Obstacle;
			obs.cameraShake = cameraShake;
		}
	}

	Obstacle GetRandomObstaclePrefab() {
		return obstacles[Random.Range(0, obstacles.Length)];
	}

}
