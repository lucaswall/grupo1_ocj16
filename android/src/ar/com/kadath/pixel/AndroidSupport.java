
package ar.com.kadath.pixel;

import android.media.MediaRecorder;
import android.util.Log;

public class AndroidSupport {

	private static final String TAG = "Pixel:AndroidSupport";
	private MediaRecorder recorder = null;

	public void startRecording() {
		Log.i(TAG, "start recording");
		try {
			if (recorder == null) {
				recorder = new MediaRecorder();
				recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
				recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
				recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
				recorder.setOutputFile("/dev/null"); 
				recorder.prepare();
				recorder.start();
			}
		} catch ( Exception e ) {
			recorder = null;
			Log.e(TAG, "recording init failed!");
			e.printStackTrace();
		}
	}

	public void stopRecording() {
		Log.i(TAG, "stop recording");
		if ( recorder != null ) {
			recorder.stop();
			recorder.release();
			recorder = null;
		}
	}

	public int getRecordingMaxAmplitude() {
		if ( recorder != null ) {
			int amp = recorder.getMaxAmplitude();
			//Log.i(TAG, "amp = "+amp);
			return amp;
		} else {
			return 0;
		}
	}

}
